package com.banzhida.analyzer.sample;

import com.banzhida.analyzer.core.IKSegmenter;
import com.banzhida.analyzer.core.Lexeme;
import com.banzhida.analyzer.helper.TxtReader;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by sangjee on 12/2/15.
 */
public class TibetanTextDemo1 {
    public static void main(String[] args) {
        File sample = new File("/home/sangjee/IdeaProjects/IKAnalyzer/src/main/java/com/banzhida/analyzer/sample/sample.txt");
        String bo_string = TxtReader.txt2String(sample);

        // useSmart =true ，分词器使用智能切分策略， =false则使用细粒度切分
        IKSegmenter seg = new IKSegmenter(new StringReader(bo_string), true);
        try {
            Lexeme lex = seg.next();
            while (lex != null) {
                System.out.print(lex.getLexemeText() + "/");
                lex = seg.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
