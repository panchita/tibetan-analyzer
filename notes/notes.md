# Register JetBrains SoftWares including InteliJ Idea

http://idea.lanyus.com 

# IKAnalyzer
https://github.com/yozhao/IKAnalyzer

# Move dictionary files to specific directory
https://github.com/wenerme/IKAnalyzer

# Much to dig in 
https://github.com/huaban/jieba-analysis
- 基于 trie 树结构实现高效词图扫描
- 生成所有切词可能的有向无环图 DAG
- 采用动态规划算法计算最佳切词组合
- 基于 HMM 模型，采用 Viterbi (维特比)算法实现未登录词识别

# dictionary path
com.banzhida.analyzer.cfg.DefaultConfig -> main dictionary path --> need to be set in pom.xml
--> like:
```xml
<resources>
<resource>
<directory>src/main/java/com/banzhida/analyzer/dic</directory>
<targetPath>com/banzhida/analyzer/dic</targetPath>
<filtering>true</filtering>
<includes>
<include>*.dic</include>
</includes>
</resource>
</resources>
```

# A good reference
- https://github.com/tibetan-nlp/tibetan-document-processing-with-solr.git
- https://github.com/tibetan-nlp/lucene-analyzers.git